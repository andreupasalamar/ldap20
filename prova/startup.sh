#! /bin/bash
ulimit -n 1024
echo "startup: $*"
case $1 in
  dia)
    /usr/bin/date
    ;;
  calendari)
    /usr/bin/cal
    ;;
  llista)
    /usr/bin/ls
    ;;
  *)
    /usr/bin/date
    ;;
esac
exit 0
