# LDAP Server
## @edt ASIX M06-ASO Curs 2020 - 2021
### Servidor LDAP
ASIX M06-ASO Escola del treball de barcelona

Imatge:

* **edtasixm06/ldap20:entrypoint** Imatge amb usuaris identificats pel uid
  per exemple uid=pere,ou=usuaris,dc=edt,dc=org. Varies opcions d'arrencada segons el valor que 
  passem: start, initdbedt, initdb
