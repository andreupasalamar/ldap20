# ldap20:schema
## ASIX M06-ASO @edt Curs 2020-2021
Servidor ldap amb futbolistes per practicar schema

Configuració client ldap.conf:

	BASE    dc=edt,dc=org
	URI     ldap://ldap.edt.org

Descarregar la imatge:
'docker pull andreupasalamar/ldap20:schema'

Executar en mode interactiu:
'docker run --name ldap.edt.org -h ldap.edt.org --net 2hisix -p 389:389 -it andreupasalamar/ldap20:schema /bin/bash'

Executar en mode detach:
'docker run --name ldap.edt.org -h ldap.edt.org --net 2hisix -p 389:389 -d andreupasalamar/ldap20:schema'
