# ldap20:acl

isx20612296@edt 
ASIX M06-ASO

servidor base amb fitxers d'exemple per inserir dinàmicament acls diferencts i practicar-les


Configuració client ldap.conf:

        BASE    dc=edt,dc=org
        URI     ldap://ldap.edt.org


Descarregar la imatge:
'docker pull andreupasalamar/ldap20:acl'

Executar en mode interactiu:
'docker run --name ldap.edt.org -h ldap.edt.org --net 2hisix -p 389:389 -it andreupasalamar/ldap20:acl /bin/bash'

Executar en mode detach:
'docker run --name ldap.edt.org -h ldap.edt.org --net 2hisix -p 389:389 -d andreupasalamar/ldap20:acl'

Exemples de ACL:

access to * by * read
access to * by * write
access to * 
	by self write 
	by * read

access to *
        by * read
        by self write

access to attrs=homePhone by * read
access to * by * write

access to * by * write
access to attrs=homePhone by * read

access to attrs=homePhone
	by dn.exact="cn=Anna Pou,ou=usuaris,dc=edt,dc=org" write
	by * read
access to * by * write

access to attrs=homePhone
	by dn.exact="cn=AnnaPou,ou=usuaris,dc=edt,dc=org" write
access to attrs=homePhone
	by * read
access to * by * write

access to attrs=homePhone
	by dn.exact="cn=Anna Pou,ou=usuaris,dc=edt,dc=org"
	by dn.exact="cn=Admin System,ou=usuaris,dc=edt,dc=org"
	by * read
access to *
	by dn.exact="cn=Admin System,ou=usuaris,dc=edt,dc=org" write
	by self write
	by * read

access to attrs=userPassword
	by self write
access to * by * read
