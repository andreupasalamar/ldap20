# ldap20:editat
## ASIX M06-ASO @edt Curs 2020-2021
Servidor ldap bàsic amb una extensió d'usuaris i simplificació del procès de creació

Configuració client ldap.conf:

	BASE    dc=edt,dc=org
	URI     ldap://ldap.edt.org

Descarregar la imatge:
'docker pull andreupasalamar/ldap20:editat'

Executar en mode interactiu:
'docker run --name ldap.edt.org -h ldap.edt.org --net 2hisix -p 389:389 -it andreupasalamar/ldap20:editat /bin/bash'

Executar en mode detach:
'docker run --name ldap.edt.org -h ldap.edt.org --net 2hisix -p 389:389 -d andreupasalamar/ldap20:editat'
