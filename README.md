# ldap20
## ASIX M06-ASO @edt Curs 2020-2021

@edt ASIX M06-ASO
Curs 2020 - 2021
* andreupasalamar/ldap20:base Imatge base de un servidor ldap que funciona amb detach. carrega edt.org, els elements bàsics i els usuaris bàsics.
	`docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisix -p 389:389 -d andreupasalamar/ldap20:base`
	`docker build -t andreupasalamar/ldap20:base .`
	
* andreupasalamar/ldap20:editat	Imatge com la base però simplificant el procés POPULATE i encriptant la password de Manager
	`docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisix -p 389:389 -d andreupasalamar/ldap20:editat`
	`docker build -t andreupasalamar/ldap20:editat .`
=======
